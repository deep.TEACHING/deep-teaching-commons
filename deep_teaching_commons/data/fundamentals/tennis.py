import os
import shutil

# third party
import requests

# internal
from deep_teaching_commons import config


class Tennis:
    def __init__(self, base_data_dir=None, auto_download=True, verbose=True):
        self.base_data_dir = base_data_dir if base_data_dir else config.BASE_DATA_DIR
        self.data_url_players = "https://gitlab.com/deep.TEACHING/educational-materials/raw/master/datasets/tennis_players.npy"
        self.data_url_games = "https://gitlab.com/deep.TEACHING/educational-materials/raw/master/datasets/tennis_games.npy"


        self.verbose = verbose

        self.data_dir = os.path.join(self.base_data_dir, 'tennis')
        self.data_path = []
        self.data_path.append(os.path.join(self.data_dir, 'tennis_players.npy'))
        self.data_path.append(os.path.join(self.data_dir, 'tennis_games.npy'))

        if auto_download:
            if self.verbose:
                print('auto download is active, attempting download')
            self.download()

    def download(self):
        # download data if directory does not yet exist
        if os.path.exists(self.data_dir):
            if self.verbose:
                print('data directory already exists, no download required')
        else:
            if self.verbose:
                print('data directory does not exist, starting download')

            # create directory
            os.makedirs(self.data_dir)

            for i, url in enumerate([self.data_url_players, self.data_url_games]):
                try:
                    # stream download
                    r = requests.get(url, stream=True)
                    r.raise_for_status()
                    with open(self.data_path[i], 'wb') as f:
                        for chunk in r.iter_content(chunk_size=4096):
                            if chunk:
                                f.write(chunk)
                    r.raise_for_status()
                except:
                    shutil.rmtree(self.data_dir)
                    raise

            if self.verbose:
                print('data successfully downloaded')
                
    def local_url_tennis_players(self):
        return self.base_data_dir + "/tennis/tennis_players.npy"
    
    def local_url_tennis_games(self):
        return self.base_data_dir + "/tennis/tennis_games.npy"
